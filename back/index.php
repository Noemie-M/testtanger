<?php
    $bool = false;

    if(isset($_POST['start'])&& isset($_POST['end'])&& isset($_POST['ammount'])&&isset($_POST['percentage'])){
        // Affiche le résultat si bool est true
        $bool = true;

        // Récuparation des variables POST
        $start = new DateTime($_POST['start']);
        $end  = new DateTime($_POST['end']);
        $percentage = $_POST['percentage'];
        $ammount = $_POST['ammount'];

        // Ajout de 1 jour à la date de fin pour le calcul de l'intervalle
        $end->add(new DateInterval('P1D'));


        // Innitialisation des variables utiles
        $numberOfWeekEndDay = 0;
        $numberOfWeekDay = 0;
        $sub_ammount = 0;
        $key = 0;

        // Calcul de l'intervalle
        $interval = $start->diff($end);
        $gap = new DateInterval('P1D');

        // Calcul de la période
        $period = new DatePeriod($start, $gap, $end);

        // Calcul du nombre de jour non nul 
        foreach($period as $date){
            $numberOfTheDay = $date->format('w');
            if($numberOfTheDay == 6 || $numberOfTheDay == 0){
                $numberOfWeekEndDay++;
            }
        }
        $numberOfWeekDay = $interval->format('%a') - $numberOfWeekEndDay;


        // Calcul de la valeur minimum 
        // ((total / pourcentage)*nombre de jour non nul) puis total / resultat
        $TEMP_value = ($ammount/$percentage)*$numberOfWeekDay;
        $minValue = $ammount / $TEMP_value;
        $minValue = floor($minValue);
        
       
        $numbers = array();
        for($i=0;$i !=$numberOfWeekDay;$i++){
            array_push($numbers,$minValue);
        }
        $sub_ammount = array_sum($numbers);
        $TEMP_ammount = $ammount - $sub_ammount;
        $j =count($numbers)-1;
        for($i = 1; $i < $numberOfWeekDay; $i++) {
            $random = mt_rand(1, $TEMP_ammount-($numberOfWeekDay-$i))/100;
            $numbers[$i] = $numbers[$i] + $random;
            $TEMP_ammount = $TEMP_ammount - $random;
            // Pour arriver à la valeur total
            if($i==$j){
                $rest=$ammount - array_sum($numbers);
                $numbers[$j]= $numbers[$j] +$rest;
            }
        }

        

        foreach($period as $date){            
            $dates[$key] = $date->format('Y-m-d');
            if($date->format('w') == 6 || $date->format('w') == 0){                
                array_splice($numbers, $key, 0, "0");         
            }
            $key++;
        }
        
        $result=array_combine($dates, $numbers);

        $ammountArray = array_sum($numbers);
        
// Tableau prend la valeur minimale réparti partout
// Ajoute un nombre aléatoire du reste

    }
?>
<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
    .card{
        width: auto;
        margin: 10%;
    }
    </style>
    <title>Randomless budget Generator</title>
  </head>
  <body>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-4"> 
            <h1 class="text-danger text-center">Randomless budget Generator</h1>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="examplePercentageRandomless">Percentage of randomless</label>
                    <input required name="percentage" type="number" min="10" max="100" class="form-control" id="examplePercentageRandomless" aria-describedby="percentageHelp">
                    <small id="percentageHelp" class="form-text text-muted">Choose a number between 10% and 100%</small>
                </div>
                <div class="form-group">
                    <label for="exampleAmmount">Total Ammount</label>
                    <input required name="ammount" type="number" min="100" class="form-control" id="exampleAmmount">
                    <small id="ammountHelp" class="form-text text-muted">Choose a ammount greater than 100$</small>
                </div>
                <div class="form-group">
                    <label for="exampleStart">Departure date</label>
                    <input type="date" name="start" class="form-control" id="exampleStart">
                </div>
                <div class="form-group">
                    <label for="exampleEnd">Arrival date</label>
                    <input type="date" name="end" class="form-control" id="exampleEnd">
                </div>
                <button type="submit" class="btn btn-danger">Calculate</button>
                </form>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-6">
            <?php if($bool){?>
            
                <div class="card mb-3">
                <div class="card-header text-white bg-danger ">Result</div>
                <div class="card-body">
                    <h5 class="card-title">All the result under.</h5>
                    <p class="card-text">Departure date: <?=$start->format('d-m-Y')?></p>
                    <p class="card-text">Arrival date:   <?=$end->sub(new DateInterval('P1D'))->format('d-m-Y')?></p>
                    <p class="card-text">Amount asked:  <?=$ammount?></p>
                    <p class="card-text">Percentage of hasard:     <?=$percentage?></p>
                    <p class="card-text">Number of weekend day: <?=$numberOfWeekEndDay?></p>
                    <p class="card-text">Minimal value per day:  <?=$minValue?></p>
                    <p class="card-text">Array: <br> 
                    <?php foreach($result as $showDate => $showNumber){?>
                        <span class="dateStyle"><?=$showDate?></span> has a budget of <span class="numberStyle"><?=$showNumber?>$</span><br>
                    <?php } ?>
                    </p>
                    <p class="card-text">New amount:    <?=$ammountArray?></p>
                </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  </body>
</html>